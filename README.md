# A Node uptime monitor

Forked from: [qawemlilo/node-ping](https://github.com/qawemlilo/node-ping)
and changed to support `https` instead of `http`. Also changed that the monitor
don't stop on `404` and changed http server to display messages.

Node-ping is a simple app that monitors the availabilty of your websites.

**Note:** Node-ping uses [Node Mailer](https://github.com/andris9/Nodemailer)
to send down-time nofication emails. Node Mailer supports many different
transports, for instance Gmail. It is a good idea to create an
[application specific password](https://accounts.google.com/IssuedAuthSubTokens)
on your Gmail account and update the `config.json` file.


## How it works

1. Download the repo: `git clone https://github.com/gizur/node-ping.git`.

2. Create `config.json` by copying `config.json.template`

3. List all websites that you want to monitor in `websites.json` and run
`node app` command to start monitoring your websites
(copy `websites.json.template` ).

4. Start with `PORT=3000 node app.js`.
 * Or use forever: `npm install forever -g`
 * `PORT=3000 forever start app.js`


## Dependencies
 - [Node Mailer](https://github.com/andris9/Nodemailer) - for sending emails


## License

MIT License
