var nodemailer = require('nodemailer'),
    config = require('./config'),
    mailer;

mailer = function (opts, fn) {

    var mailOpts, smtpTrans;

    try {
        smtpTrans = nodemailer.createTransport('SMTP', {
            service: 'Gmail',
            auth: {
                user: config.GmailAuth.email,
                pass: config.GmailAuth.password
            }
        });
    }
    catch (err) {
        fn('Nodemailer could not create Transport', '');
        return;
    }

    mailOpts = {
        from: opts.from,
        replyTo: opts.from,
        to: opts.to,
        subject: opts.subject,
        html: opts.body
    };

    try {
        smtpTrans.sendMail(mailOpts, function (error, response) {
            if (error) {
                fn(true, error);
            }
            else {
                fn(false, response.message);
            }
        });
    }
    catch (err) {
        fn('Nodemailer could not send Mail', '');
    }
};

module.exports = mailer;
