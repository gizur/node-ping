var Monitor = require('./monitor.js'),
  websites = require('./websites'),
  events = require('./events'),
  http = require('http');


// Collect stdout and display in web server
// -----------------------------------------

var logs = [];
var logLength = 100;
var port = process.env.PORT || 8125;

console.log('Server running at http://127.0.0.1:' + port);

// put stdout in an array so we can show it a web server
process.stdout.write = logs.unshift.bind(logs);

http.createServer(function(request, response) {
  response.write('<script>setInterval(location.reload.bind(location), 60*1000)</script>');

  if(logs.length > logLength) logs.splice(logLength, logs.length-logLength);
  logs.forEach(response.write.bind(response));
  response.end();
}).listen(port);


// Setup the monitoring
// -----------------------------------------

websites.forEach(function(website) {
  var monitor = new Monitor({
    website: website.url,
    timeout: website.timeout
  });

  monitor.on('error', events.onError);
  monitor.on('start', events.onStart);
  monitor.on('stop', events.onStop);
  monitor.on('down', events.onDown);
  monitor.on('up', events.onUp);
});
