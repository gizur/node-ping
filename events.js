var config = require('./config.json'),
  mailer = require('./mailer');

function getFormatedDate(time) {
  var currentDate = new Date(time);

  currentDate = currentDate.toISOString();
  currentDate = currentDate.replace(/T/, ' ');
  currentDate = currentDate.replace(/\..+/, '');

  return currentDate;
};

function onStart(website) {
  var time = Date.now();

  console.log(website.statusMessage + " (" + website.statusCode + ") " +
    website.website + " " + getFormatedDate(time) + " UTC " +
    "<br/>");
};

function onStop(website) {
  mailer({
      from: config.GmailAuth.email,
      to: config.sendToAddress,
      subject: website + ' monitor has stopped',
      body: '<p>' + website + ' is no longer being minitored.</p>'
    },

    function(error, res) {
      if (error) {
        console.log('Failed to send email');
      } else {
        console.log(res.message);
      }
    });
};

function onDown(res) {
  var msg = '';

  msg += '<p>Time: ' + res.time;
  msg += '</p><p>Website: ' + res.website;
  msg += '</p><p>Message: ' + res.statusMessage + '</p>';

  mailer({
      from: config.GmailAuth.email,
      to: config.sendToAddress,
      subject: res.website + ' is down',
      body: msg
    },

    function(error, res) {
      if (error) {
        console.log('Failed to send email');
      } else {
        console.log(res.message);
      }
    });

  var time = Date.now();

  console.log(res.statusMessage + " (" + res.statusCode + ") " +
    res.website + " " + getFormatedDate(time) + " UTC " +
    "<br/>");

};

module.exports.onStart = onStart;
module.exports.onStop = onStop;
module.exports.onUp = onStart;
module.exports.onDown = onDown;
module.exports.onError = onDown;
