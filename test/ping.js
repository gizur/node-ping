var Monitor = require('../monitor.js'),
    should = require('should'),
    website = {website: 'https://gizur.com', timeout: 0.2},
    config;


describe('Monitor', function() {
    describe('#Monitor', function() {
        it('should be start monitoring https://gizur.com', function(done) {
            var monitor = new Monitor(website);

            monitor.timeout.should.be.eql(0.2 * (60 * 1000));
            monitor.website.should.be.eql('https://gizur.com');

            monitor.stop();

            done();
        });
    });
});
